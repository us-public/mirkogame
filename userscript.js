var mirko = () => {
  'use strict';

  (document.head || document.documentElement).insertAdjacentHTML('beforeend',
    `
      <style>
        aside.toolbox { position: fixed; left: 20px; bottom: 20px; background: rgb(77 181 34); padding: 10px; }
        aside.toolbox > button { margin: 20px 0; }
        
        aside.toolbox-fleet * {
            user-select: none;
        }
        
        aside.toolbox-fleet { 
            position: fixed;
            left: 20px;
            bottom: 20px;
            background: #4545458f;
            padding: 0 10px;
            /*width: 650px;*/
            overflow-y: auto;
            max-height: 50vh;
        }
        
         aside.toolbox-fleet #toolbox-content.hide {
            display: none;
         }
         
         aside.toolbox-fleet table {
            margin: 0;
         }
        
        aside.toolbox-fleet .filters {
          position: sticky;
          top: 0;
          left: 0;
          padding: 10px;
          width: calc(100% - 40px);
          background: rgb(29 34 54);
        }
        
        aside.toolbox-fleet .filters  fieldset {
           display: inline;
        }
        
        aside.toolbox-fleet .buttons {
          position: sticky;
          bottom: 0;
          left: 0;
          padding: 10px;
          width: calc(100% - 40px);
          background: rgb(29 34 54);
        }
        
        aside.toolbox-fleet .buttons button { 
            margin: 0;
            display: inline;
        }
        aside.toolbox-fleet .buttons button.btn-collect {
            background: #ff5200ba;
        }
        aside.toolbox-fleet .buttons button.btn-attack {
            background: #ff2600;
        }
        .tab-planets tr td:nth-child(2) {
            text-align: left;
        }
        
       
       /*a[href*="cp=6026"] img { display: none!important; }*/
       /*a[href*="cp=6026"] { display: block; width:80px; height:80px; background-image:url(./styles/theme/epicblue_old/planeten/small/s_wuestenplanet02.jpg); background-size: 80px 80px; margin: 0 auto;  }*/
       /*.tooltip_sticky[data-tooltip-content*="La Roca"] { background-image:url(./styles/theme/epicblue_old/planeten/small/s_wuestenplanet02.jpg); }*/

       /*!* nie dziala bo jest dynamicznie doczytywana *!*/
       /*#pbox div[onclick*="cp=6026"] > .pboxpic { background-image:url(./styles/theme/epicblue_old/planeten/small/s_wuestenplanet02.jpg); }*/
       /*img[alt*="La Roca"] { background-image:url(./styles/theme/epicblue_old/planeten/wuestenplanet02.jpg); }*/
     </style>`
  );

  var DateTime = luxon.DateTime;
  // rxjs.timer(1000,1000).subscribe(x => {
  //   // showPlanetToolbox();
  // })

  setTimeout(() => {
    showPlanetToolbox();
    showToolbox(true);
    // poprawic wersje dla szpiegowania + zapisywanie kiedy wroci flota i dzielenie current / 2

    if (window.location.href.includes('game.php?page=fleet')) {
      ['attack', 'spy'].forEach(type => {
        [1, 2, 3, 4].forEach(x => detectStep(type, x))
      });
    } else {
      console.warn('to nie widok floty!!')
    }
    if (window.location.href.includes('game.php?page=messages')) {
      setTimeout(() => {
        window.grabData();
        const targetNode = document.querySelector('#content');
        const observer = new MutationObserver((mutationsList, observer) => {
          // Use traditional 'for loops' for IE 11
          for (const mutation of mutationsList.slice(0, 1)) {
            window.grabData();
          }
        });
        observer.observe(targetNode, { attributes: false, childList: true, subtree: false });
      }, 1000)
    }


  },200)

  function showPlanetToolbox() {

    let body = `
      <div class="filters">
        <fieldset>
        <legend>Sort by:</legend>
          <button onclick="window.sortBySumRank()">Suma</button>
          <button onclick="window.sortByMetal()">Met</button>
          <button onclick="window.sortByCristal()">Kris</button>
          <button onclick="window.sortByDeuterium()">Deu</button>
          <button onclick="window.sortByDate()">Data</button>
          <button onclick="window.sortByCoords()">Coordsy</button>
        </fieldset>
        <fieldset>
        <legend>Filter:</legend>
            <button onclick="window.toggleDefense()">Toggle defense</button>
        </fieldset>
        <fieldset style="padding-right: 20px">
        <legend>Action:</legend>
          <button onclick="window.spyAll()">Spy sel.</button>
          <button onclick="window.attackAll()">Attack sel.</button>
          <button onclick="document.getElementById('checkbox-number').value = 0; window.checkMany(0)" style="margin-right: -10px">Zero</button>
          <input id="checkbox-number" style="width: 25px; background: #114cb9; margin-right: -10px" value="5" />
          <button onclick="document.getElementById('checkbox-number').value = Number(document.getElementById('checkbox-number').value) + 1; window.checkMany(document.getElementById('checkbox-number').value)" style="margin-left: 7px">+</button>
          <button onclick="document.getElementById('checkbox-number').value = Number(document.getElementById('checkbox-number').value) - 1; window.checkMany(document.getElementById('checkbox-number').value)" style="margin-left: -7px">-</button>
        </fieldset>
        <button onclick="window.refresh()">REFRESH</button>
      </div>
      <table class="tab-planets">
          <tbody>
           `;

  let state = JSON.parse(localStorage.getItem('planets')) ?? [];
  let sortBy = localStorage.getItem('sortBy') ?? 'sumRank';
  let showDefence = JSON.parse(localStorage.getItem('show_defence')) ?? false;

  state = state.filter(x => showDefence ? true : x.anyDefense === 0 || x.onlyRockets);

  if (sortBy === 'sumRank') {
    state = state.sort((x, y) => y.resources.sum - x.resources.sum);
  } else if (sortBy === 'date') {
    state = state.sort((x, y) => new Date(y.date) - new Date(x.date));
  } else if (sortBy === 'coords') {
    state = state.sort((x, y) => (Math.abs(x.planetArr[0] + x.planetArr[1] - 9)) - Math.abs(y.planetArr[0] + y.planetArr[1] - 9));
  } else if (sortBy === 'metal') {
    state = state.sort((x, y) => y.resources.current['Metal'] - x.resources.current['Metal']);
  } else if (sortBy === 'cristal') {
    state = state.sort((x, y) => y.resources.current['KrysztaĹ'] - x.resources.current['KrysztaĹ']);
  } else if (sortBy === 'deuterium') {
    state = state.sort((x, y) => y.resources.current['Deuter'] - x.resources.current['Deuter']);
  }


    for (let planet of state) {
      const date = DateTime.fromISO(planet.date);
    body += `
              <tr>
                  <td>${date.toRelative()}</td>
                  <td>${planet.user.slice(0, 10)}</td>
                  <td>${planet.planet}</td>
                  <td ${!!planet.lastAttackReport ? 'style="color:#f78787"' : ''}>
                     <div style="display: grid; grid-template-columns: repeat(3, 1fr); gap: 2px; text-align: left">
                      <div>
                        <img src="./styles/theme/gow/gebaeude/901.gif" alt="Metal" width="10" height="10">
                        <span>
                        ${ planet.resources.current['Metal']} 
  <!--                      (${Math.floor(planet.resources.current['Metal'] / planet.resources.max['Metal'] * 100)}%)-->
                        </span>
                      </div>
                      <div>
                        <img src="./styles/theme/gow/gebaeude/902.gif" alt="Metal" width="10" height="10">
                        <span>
                        ${ planet.resources.current['KrysztaĹ']} 
  <!--                      (${Math.floor(planet.resources.current['KrysztaĹ'] / planet.resources.max['KrysztaĹ'] * 100)}%)-->
                        </span>
                      </div>
                      <div>
                        <img src="./styles/theme/gow/gebaeude/903.gif" alt="Metal" width="10" height="10">
                        <span>
                        ${planet.resources.current['Deuter']} 
  <!--                      (${Math.floor(planet.resources.current['Deuter'] / planet.resources.max['Deuter'] * 100)}%)-->
                        </span>
                      <div>
                    </div>
                  </td>`
                  if (planet.anyDefense > 0 && planet.onlyRockets) {
                    body += `<td style="color:#dec273">${planet.anyDefense}</td>`
                  } else if(planet.anyDefense > 0) {
                    body += `<td><span style="color:#EE5C5C">${planet.anyDefense}</span></td>`
                  } else {
                    body += `<td style="color:#aadd93">0</td>`
                  }
    body += `     <td>
                    <input class="action-checkbox" type="checkbox" id="${planet.planet}"/>
                  </td>
                  <td>
                    ${planet.lastAttacks.length}
                  </td>
                  <td>
                      <button class="btn-spy" onclick="window.action('spy', '${planet.planet}', null)">Spy</button>
                      <button class="btn-attack" onclick="window.action('attack', '${planet.planet}', null)">Atak</button>
                      <button class="btn-attack" onclick="window.multipleAttackFor('${planet.planet}')">Rozgrab</button>
                  </td>`
  }

  body += `
              </tr>
          </tbody>
      </table>
      <div class="buttons">
        <button class="btn-collect" onclick="window.grabData()">ZBIERZ DANE</button>
        <button class="btn-collect" onclick="window.updateAttacks()">AKTUALIZUJ ATAKI</button>
      </div>
    `;
    const toolbox = document.querySelector('.toolbox-fleet');
    if (toolbox) {
      toolbox.innerHTML = body;
    } else {
      document.body.insertAdjacentHTML('beforeend', `
        <aside class="toolbox-fleet">
           <button onclick="window.showToolbox()">On/Off</button>
           <div id="toolbox-content" style="position:relative;">${body}</div>
        </aside>
      `);
    }
  }

  function getSelectedPlanets() {
    return Array.from(document.querySelectorAll('.action-checkbox')).filter(x => x.checked).map(x => x.id);
  }

  function unselectAll() {
    return Array.from(document.querySelectorAll('.action-checkbox')).forEach(node => node.checked = false)
  }

  function multipleAttackTries(planet) {
    const min = 8000;
    let number = 0;
    let copy = { ...planet.resources.current }
    do {
      copy['Metal'] /= 2;
      copy['KrysztaĹ'] /= 2;
      copy['Deuter'] /= 2;
      number++;
    } while(copy['Metal'] > min || copy['KrysztaĹ'] > min || copy['Deuter'] > min)
    return number;
  }

  window['showToolbox'] = (init) => {
    let status = JSON.parse(localStorage.getItem('toolbox_hide'));
    if (status === null) {
      status = false;
    }
    const content = document.querySelector('#toolbox-content');
    if (!init) {
      if (status) {
        content.classList.remove('hide');
      } else {
        content.classList.add('hide');
      }
      localStorage.setItem('toolbox_hide', JSON.stringify(!status));
    } else {
      if (!status) {
        content.classList.remove('hide');
      } else {
        content.classList.add('hide');
      }
    }
  }

  window['multipleAttackFor'] = (coords) => {
    const planets = JSON.parse(localStorage.getItem('planets')) ?? [];
    const planet = planets.find(x => x.planet === coords);
    if (!planet) {
      console.warn(`nie znaleziono planety ${coords} do rozgrabienia!`);
      return -1;
    }

    const multipleAttacksNum = multipleAttackTries(planet);
    const targetPlanets = Array.from({length: multipleAttacksNum}).fill(coords);
    localStorage.setItem('attack_many', JSON.stringify(targetPlanets))
    localStorage.setItem('attack_multiple_current_num', '1');
    window.action('attack',null, false);
  }
  window['spyAll'] = () => {
    localStorage.setItem('spy_many', JSON.stringify(getSelectedPlanets()))
    window.action('spy',null, false);
    unselectAll();
  }
  window['attackAll'] = () => {
    localStorage.setItem('attack_many', JSON.stringify(getSelectedPlanets()))
    window.action('attack', null, false);
    unselectAll();
  }
  window['checkMany'] = (quantity) => {
    unselectAll();
    Array.from(document.querySelectorAll('.action-checkbox')).slice(0, quantity).forEach(node => node.checked = true)
  }
  window['refresh'] = () => {
    showPlanetToolbox();
  }
  window['sortBySumRank'] = () => {
    localStorage.setItem('sortBy',  'sumRank');
    showPlanetToolbox();
  }
  window['sortByMetal'] = () => {
    localStorage.setItem('sortBy',  'metal');
    showPlanetToolbox();
  }
  window['sortByCristal'] = () => {
    localStorage.setItem('sortBy',  'cristal');
    showPlanetToolbox();
  }
  window['sortByDeuterium'] = () => {
    localStorage.setItem('sortBy',  'deuterium');
    showPlanetToolbox();
  }
  window['sortByDate'] = () => {
    localStorage.setItem('sortBy',  'date');
    showPlanetToolbox();
  }
  window['sortByCoords'] = () => {
    localStorage.setItem('sortBy',  'coords');
    showPlanetToolbox();
  }
  window['toggleDefense'] = () => {
    let showDefence = JSON.parse(localStorage.getItem('show_defence')) ?? false;
    localStorage.setItem('show_defence', !showDefence + '');
    showPlanetToolbox();
  }



  function detectStep(type, stepNumber) {
    const stepKey = type === 'attack' ? 'attack_step' : 'spy_step'
    const msgText = type === 'attack' ? 'ataku' : 'szpiegowania';

    const step = JSON.parse(localStorage.getItem(stepKey)) === stepNumber;
    if (!step) {
      console.warn(`===== Nie wykryto ${stepNumber} kroku ${msgText} =======`);
      return;
    }
    console.log(`===== Wykryto ${stepNumber} krok ${msgText} =======`);
    setTimeout(() => {
      if (stepNumber === 4) {
        localStorage.removeItem(stepKey);
        if (type === 'attack') {
          console.log('zapisuje atak');
          saveAttackData(); // zapisz dane o wyslanej flocie
        }
        setTimeout(() => {
          const error = window.action(type, null, true);
          if (error === -1) {
            window.close();
            if (type === 'attack') {
              localStorage.removeItem('attack_multiple_max_num');
            }
          }
        }, randomTime(800, 1000)) // krotki czas bo potem jest redirect
      } else {
        const button = document.querySelector('input[value="Dalej"]');
        if (button) {
          localStorage.setItem(stepKey, stepNumber + 1);
          button.click();
        }
      }
    }, randomTime(200, 600))
  }

  function saveAttackData() {
    const element = document.querySelector('#content');
    const info = element.innerText.split('\n').map(x => x.replaceAll(/\s/g, ' '));
    const regex = /[a-z]*(\d.*)$/gm;
    const regex2 = /[a-z]*(\d.*)$/gm;
    const attackData = {
      source: info[5].split(' ')[1],
      target: info[6].split(' ')[1],
      departureDate: new Date(),
      arrivalDate: getDate(regex.exec(info[7])[0]),
      returnDate: getDate(regex2.exec(info[8])[0]),
    }
    localStorage.setItem(`attack_data_${attackData.target}_${Date.now()}`, JSON.stringify(attackData));
  }

  window['action'] = (type, singleCoords, self) => {
    const actionKey = type === 'attack' ? 'attack_many' : 'spy_many'
    const stepKey = type === 'attack' ? 'attack_step' : 'spy_step'
    const msgText = type === 'attack' ? 'ataku' : 'szpiegowania';

    let planetsCoords = [];
    if (!!singleCoords) {
      planetsCoords.push(singleCoords);
    } else {
      planetsCoords = JSON.parse(localStorage.getItem(actionKey)) ?? [];
      if (planetsCoords.length < 1) {
        console.warn(`nie znaleziono planet do ${msgText}!`);
        return -1;
      }
    }

    const coords = planetsCoords[0];
    planetsCoords = planetsCoords.slice(1)
    if (planetsCoords.length > 0) {
      localStorage.setItem(actionKey, JSON.stringify(planetsCoords)) // zmniejsz tablice planet do ataku o jeden
    } else {
      localStorage.removeItem(actionKey);
    }

    const planets = JSON.parse(localStorage.getItem('planets')) ?? [];
    const planet = planets.find(x => x.planet === coords);
    if (!planet) {
      console.warn(`nie znaleziono planety ${coords} do ${msgText}!`);
      return -1;
    }

    localStorage.setItem(stepKey, '1');

    const targetMission = type === 'attack' ? 1 : 6;
    const shipNumber = type === 'attack' ? 202 : 210;  //  sonda 210, maly 202, duzy 203
    let quantity;

    const multipleAttacksCurrentNum = Number(localStorage.getItem('attack_multiple_current_num'));
    if (multipleAttacksCurrentNum) {
      const divider = multipleAttacksCurrentNum * 2;
      quantity = Math.ceil((planet.resources.sum / divider) / 5000) + 1
      quantity = Math.ceil(quantity * 1.1); // wyslij jednak o 10% wiÄcej
      localStorage.setItem('attack_multiple_current_num', (multipleAttacksCurrentNum + 1).toString());
    } else {
      quantity = type === 'attack' ? Math.ceil((planet.resources.sum / 2) / 5000) + 1 : 1;
    }

    console.log(`otwieram okno na kolejny step`);
    window.open(
        `https://mirkogame.pl/game.php?page=fleetTable&galaxy=${planet.planetArr[0]}&system=${planet.planetArr[1]}&planet=${planet.planetArr[2]}&planettype=1&target_mission=${targetMission}&ship[${shipNumber}]=${quantity}`,
        self ? '_self' : '_blank'
    )
  }

  window['clearData'] = () => {
    localStorage.removeItem('planets');
    showPlanetToolbox();
  }

  window['grabData'] = () => {
    let state = JSON.parse(localStorage.getItem('planets'));
    if (!state) {
      state = [];
    }

    const spyReports = Array.from(document.querySelectorAll('.spyRaport')).map(x => getPlanetData(x))
    const fightResults = Array.from(document.querySelectorAll('.raportMessage')).map(x => getFightResult(x))

    for (let spyReport of spyReports) {
      const index = state.findIndex(x => x.planet === spyReport.planet);
      if (index > -1) {
        if (new Date(state[index].date) < new Date(spyReport.date)) {
          state[index] = spyReport;
        }
      } else {
        state.push(spyReport);
      }
    }

    for (let attackReport of fightResults) {
      const index = state.findIndex(x => x.planet === attackReport.planet);
      if (index > -1 && new Date(state[index].date) < new Date(attackReport.date)) {
        state[index].date = attackReport.date; // change date
        state[index].lastAttackReport = attackReport;
      }
    }

    // const overwriteMerge = (destinationArray, sourceArray, options) => sourceArray
    // localStorage.setItem('planets', JSON.stringify(deepmerge(state, newState, { arrayMerge: overwriteMerge })));
    localStorage.setItem('planets', JSON.stringify(state.sort((a,b) => ('' + a.planet).localeCompare(b.planet))));
    showPlanetToolbox();
  };

  window['updateAttacks'] = () => {
    let state = JSON.parse(localStorage.getItem('planets'));
    if (!state) {
      state = [];
    }

    const toUpdate = [];
    Object.keys(localStorage).filter(x => x.startsWith('attack_data_')).forEach(x => {
      toUpdate.push(JSON.parse(localStorage.getItem(x)));
      localStorage.removeItem(x);
    })

    for (let attack of toUpdate) {
      const index = state.findIndex(x => x.planet === attack.target);
      if (index > -1) {
        state[index].lastAttacks.push(attack)
      }
    }

    localStorage.setItem('planets', JSON.stringify(state.sort((a,b) => ('' + a.planet).localeCompare(b.planet))));
    showPlanetToolbox();
  };

  function randomTime(min, max) {
    if (!min) {
      min = 1500;
    }
    if (!max) {
      max = 3000;
    }
    return Math.floor((Math.random() * (max - min) + 1) + min);
  }

  const createObj = (array) => {
    return array.reduce((acc, curr, i, arr) => {
      if (i % 2 === 0) acc[curr] = Number(arr[i + 1].replaceAll('.',''));
      return acc;
    }, {});
  };

  function getDate(input) {
    //const input = '19. listopad 2020, 10:04:25';
    const regex = /^([0-9]+)\. ([a-z]*) ([0-9]+),\ (.*)$/;
    let [_, day, month, year, time] = regex.exec(input);
    const monthValues = ['styczeĹ', 'luty', 'marzec', 'kwiecieĹ', 'maj', 'czerwiec', 'lipiec', 'sierpieĹ', 'wrzesieĹ', 'paĹşdziernik', 'listopad', 'grudzieĹ'];
    const monthValuesNum = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    const index = monthValues.findIndex(x => x === month);
    const output = `${year}-${monthValuesNum[index]}-${day}T${time}`;
    return new Date(output);
  }

  function getPlanetData(node) {
    // "Raport szpiegowski Planeta macierzysta (Gracz: Fiodooor) [4:17:6] na 18. listopad 2020, 13:07:32"
    const info = node.children[1].children[0].children[0].innerText;
    const regex = /^Raport szpiegowski (.*) \(Gracz: (.*)\) \[(.*)\] na (.*)$/gm;
    let [_, name, user, planet, date] = regex.exec(info);

    const currentResources = createObj(node.children[3].innerText.split('\n').slice(1, -2).map(x => Number(x) ? x.replace('.', '') : x));
    const ships = createObj(node.children[4].innerText.split('\n').slice(1));
    const defense = createObj(node.children[5].innerText.split('\n').slice(1));

    const buildingsAndResearches = node.children[6].innerText.split('\n').slice(1).slice(0, -5).filter(x => x !== 'Badania');
    const buildings = createObj(buildingsAndResearches.slice(0, 38));
    const researches = createObj(buildingsAndResearches.slice(38));

    const anyDefense = toArray(ships).reduce((acc,curr) => acc + curr) + toArray(defense).reduce((acc,curr) => acc + curr);
    const onlyRockets = anyDefense === defense['Przeciwrakieta'] + defense['Rakieta MiÄdzyplanetarna'];

    return {
      planet,
      planetArr: planet.split(':'),
      name,
      user,
      date: getDate(date),
      resources: {
        current: currentResources,
        perHour: resourcesPerHour(currentResources, buildings, researches),
        max: resourcesCapacity(currentResources, buildings),
        afterLastFight: null,
        sumRank: Object.keys(currentResources).map(key => currentResources[key]).reduce((acc, curr, index) => acc + (curr * (index + 1)), 0),
        sum: Object.keys(currentResources).map(key => currentResources[key]).reduce((acc, curr, index) => acc + curr, 0),
      },
      anyDefense: toArray(ships).reduce((acc,curr) => acc + curr) + toArray(defense).reduce((acc,curr) => acc + curr),
      onlyRockets,
      ships,
      defense,
      buildings,
      researches,
      lastAttackReport: null,
      lastAttacks: [],
    };
  }


   // TODO
  function getFightResult(node) {
    const info = node.children[0].children[0].innerText;
    const infoArr = info.split('\n')

    // "Raport walki [4:39:7] (P)"
    const regex = /^Raport walki \[(.*)\] \((\S)\)$/gm;
    const [_, planet, result] = regex.exec(infoArr[0]);

    // "Profit	Metal: 28.711 KrysztaĹ: 8.085 Deuter: 880" (zamiast spacji sa dziwne biale znaki - stad replace)
    const resString = infoArr[2].slice(7).replaceAll(/\s/g, ' ');
    const regex2  = /Metal: (\S*) KrysztaĹ: (\S*) Deuter: (\S*)$/gmu;
    const [_x, met, kris, deu] = regex2.exec(resString);

    const msgId = Array.from(node.parentNode.parentNode.classList)[1];
    const header = document.querySelector(`#${msgId}`)
    const date = getDate(header.children[1].innerText)

    return {
      planet,
      won: result === 'P',
      date,
      resources: {
        'Metal': Number(met.replaceAll('.','')),
        'KrysztaĹ': Number(kris.replaceAll('.','')),
        'Deuter': Number(deu.replaceAll('.','')),
      },
    }
  }

  function toArray(o) {
    return Object.keys(o).map(key => o[key]);
  }

  function resourcesCapacity(resources, buildings) {
    const tmp = {};
    const lvls = {
      'Metal': buildings['Magazyn Metalu'],
      'KrysztaĹ': buildings['Magazyn KrysztaĹu'],
      'Deuter': buildings['Zbiornik Deuteru'],
    };
    const capacity = [16000, 32000, 64000, 120000, 224000, 408000, 752000, 1384000, 2544000, 4672000, 8568000, 15712000, 28808000, 52808000, 96816000, 177480000, 325360000, 596456000];
    for (let key in resources) {
      tmp[key] = capacity[lvls[key]];
    }
    return tmp;
  }

  function resourcesPerHour(resources, buildings, researches) {
    const tmp = {};
    const lvls = {
      'Metal': buildings['Kopalnia Metalu'],
      'KrysztaĹ': buildings['Kopalnia KrysztaĹu'],
      'Deuter': buildings['Ekstraktor Deuteru'],
    };
    const extraProduction = {
      'Metal': researches['Technologia Produkcji Metalu'],
      'KrysztaĹ': researches['Technologia Produkcji KrysztaĹu'],
      'Deuter': researches['Technologia Produkcji Deuterium'],
    };
    for (let key in resources) {
      tmp[key] = productionPerHour(key, lvls[key]) * (1 + Math.ceil(extraProduction[key] * 0.02));
    }
    return tmp;
  }

  function productionPerHour(res, lvl) {
    const factors = {
      'Metal': 150,
      'KrysztaĹ': 100,
      'Deuter': 55,
    };
    const initProduction = {
      'Metal': 100,
      'KrysztaĹ': 50,
      'Deuter': 0,
    };
    return lvl === 0 ? initProduction[res] : Math.ceil(factors[res] * lvl * Math.pow(1.1, lvl));
  }

  const shipsDefault = {
    'MaĹy Transporter': 0,
    'DuĹźy Transporter': 0,
    'Lekki MyĹliwiec': 0,
    'CiÄĹźki MyĹliwiec': 0,
    'KrÄĹźownik': 0,
    'OkrÄt Wojenny': 0,
    'Statek Kolonizacyjny': 0,
    'Recykler': 0,
    'Sonda Szpiegowska': 0,
    'Bombowiec': 0,
    'Satelita SĹoneczny': 0,
    'Niszczyciel': 0,
    'Gwiazda Ĺmierci': 0,
    'Pancernik': 0,
  }

  const defenseDefault = {
    'Wyrzutnia Rakiet': 0,
    'Lekkie DziaĹo Laserowe': 0,
    'CiÄĹźkie DziaĹo Laserowe': 0,
    'DziaĹo Gaussa': 0,
    'DziaĹo Jonowe': 0,
    'DziaĹo Plazmowe': 0,
    'MaĹa PowĹoka Ochronna': 0,
    'DuĹźa PowĹoka Ochronna': 0,
    'Przeciwrakieta': 0,
    'Rakieta MiÄdzyplanetarna': 0,
  }

  const buildingsDefault = {
    'Kopalnia Metalu': 0,
    'Kopalnia KrysztaĹu': 0,
    'Ekstraktor Deuteru': 0,
    'Elektrownia SĹoneczna': 0,
    'Elektrownia Fuzyjna': 0,
    'Fabryka RobotĂłw': 0,
    'Fabryka NanitĂłw': 0,
    'Stocznia': 0,
    'Magazyn Metalu': 0,
    'Magazyn KrysztaĹu': 0,
    'Zbiornik Deuteru': 0,
    'Laboratorium Badawcze': 0,
    'Terraformer': 0,
    'Depozyt sojuszniczy': 0,
    'Baza KsiÄĹźycowa': 0,
    'Falanga': 0,
    'Teleporter': 0,
    'Silos Rakietowy': 0,
    'Uniwersytet': 0,
  };
  const researchesDefault = {
    'Technologia Szpiegowska': 0,
    'Technologia Komputerowa': 0,
    'Technologia Bojowa': 0,
    'Technologia Ochronna': 0,
    'Technologia Opancerzenia': 0,
    'Technologia Energetyczna': 0,
    'Technologia Nadprzestrzenna': 0,
    'NapÄd spalinowy': 0,
    'NapÄd Impulsowy': 0,
    'NapÄd Nadprzestrzenny': 0,
    'Technologia Laserowa': 0,
    'Technologia Jonowa': 0,
    'Technologia Plazmowa': 0,
    'MiÄdzygalaktyczna SieÄ BadaĹ': 0,
    'Astrofizyka': 0,
    'Technologia Produkcji Metalu': 0,
    'Technologia Produkcji KrysztaĹu': 0,
    'Technologia Produkcji Deuterium': 0,
    'RozwĂłj GrawitonĂłw': 0,
  }

}